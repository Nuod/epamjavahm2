//codingbat.com/prob/p117630

import java.util.Map;
import java.util.TreeMap;

public class Map2__wordCount {

public  void Start()
{
    System.out.println("\n");
    System.out.println(wordCount(new String[] {"a", "b", "a", "c", "b"}));
    System.out.println(wordCount(new String[] {"c", "c", "c", "c"}));
    System.out.println(wordCount(new String[] {}));
}
    public Map<String, Integer> wordCount(String[] strings) {
        Map<String, Integer> res = new TreeMap<String, Integer>();
        for (int i = 0; i<strings.length;i++)
        {
            if (res.containsKey(strings[i]) == true)
            {
                int tmp = res.get(strings[i]);
                res.put(strings[i], tmp+1);
            }
            else
            {
                res.put(strings[i], 1);
            }
        }
return res;
}

}
