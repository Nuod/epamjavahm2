//codingbat.com/prob/p192570

public class String3__withoutString {

    public  void Start()
    {
        System.out.println("\n");
        System.out.println(withoutString("THIS is a FISH", "iS"));
        System.out.println(withoutString("xyzzy", "Y"));
        System.out.println(withoutString("AA22bb", "2"));
    }
    public String withoutString(String base, String remove) {
        String res = base.replaceAll(remove, "");
        res = res.replaceAll(remove.toLowerCase(), "");
        res = res.replaceAll(remove.toUpperCase(), "");

        return res;
    }

}
