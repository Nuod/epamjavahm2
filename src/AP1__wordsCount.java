//codingbat.com/prob/p124620

public class AP1__wordsCount {

    public void Start()
    {
        System.out.println("\n");
        System.out.println(wordsCount(new String[]{"a", "bb", "b", "ccc"}, 1));
        System.out.println(wordsCount(new String[]{"xx", "yyy", "x", "yy", "z"}, 2));
        System.out.println(wordsCount(new String[]{"a", "bb", "b", "ccc"}, 4));
    }
    public int wordsCount(String[] words, int len) {
        int res = 0;
        for(int i = 0;i< words.length;i++)
        {
            if(words[i].length() == len)
                res++;
        }
        return res;
    }
}
