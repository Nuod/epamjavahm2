



public class Main {

    public static void main(String[] args)
    {
        var task0 = new Warmup2__stringTimes();
        var task1 = new Logic2__luckySum();
        var task2 = new String3__withoutString();
        var task3 = new Array3__fix34();
        var task4 = new AP1__wordsCount();
        var task5 = new Functional2__no34();
        var task6 = new Map2__wordCount();
        var task7 = new Recursion2__split53();

        task0.Start();
        task1.Start();
        task2.Start();
        task3.Start();
        task4.Start();
        task5.Start();
        task6.Start();
        task7.Start();
    }
}
