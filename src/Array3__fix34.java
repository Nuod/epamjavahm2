//codingbat.com/prob/p159339

import java.util.Arrays;
import java.util.Stack;

public class Array3__fix34 {

    public void Start()
    {
        System.out.println("\n");
        System.out.println(Arrays.toString(fix34(new int[]{5, 3, 5, 4, 5, 4, 5, 4, 3, 5, 3, 5})));
        System.out.println(Arrays.toString(fix34(new int []{1})));
        System.out.println(Arrays.toString(fix34(new int[]{2, 3, 2, 3, 2, 4, 4})));
    }
    public int[] fix34(int[] nums) {
        int[] res = nums;
        Stack<Integer> stack = new Stack<>();

        for(int i=0;i<res.length;i++)
        {
            if(res[i]==4)
            {
                stack.push(i);
            }
        }
        for(int i = 0;i<res.length;i++)
        {
            if(res[i]==3)
            {
                int tmp = res[i+1], st = stack.pop();
                res[i+1] = res[st];
                res[st] = tmp;
            }
        }
        return res;
    }

}
