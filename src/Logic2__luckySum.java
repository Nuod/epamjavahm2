//codingbat.com/prob/p130788

public class Logic2__luckySum {
    public void Start()
    {
        System.out.println("\n");
        System.out.println(luckySum(1, 2, 3));
        System.out.println(luckySum(13, 2, 3));
        System.out.println(luckySum(9, 4, 13));
    }
    public int luckySum(int a, int b, int c) {
        if(a == 13)
            return 0;
        else if(b == 13)
            return a;
        else if(c == 13)
            return a+b;
        else
            return a+b+c;
    }

}
