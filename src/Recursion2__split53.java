//codingbat.com/prob/p168295

public class Recursion2__split53 {
    public  void Start()
    {
        System.out.println("\n");
        System.out.println(split53(new int[]{1, 1}));
        System.out.println(split53(new int[]{1, 1, 1}));
        System.out.println(split53(new int[]{2, 4, 2}));
    }
    public boolean split53(int[] nums) {
        return rec(nums, 0, 0, 0);
    }

    private boolean rec ( int[] nums, int index, int sum1, int sum2 ) {
        if ( index >= nums.length ) {
            return sum1 == sum2;
        }
        int tmp = nums[index];
        if (tmp % 5 == 0)
            return rec(nums, index + 1, sum1 + tmp, sum2);
        else if (tmp % 3 == 0)
            return rec(nums, index + 1, sum1, sum2 + tmp);
        else
            return (rec(nums, index + 1, sum1 + tmp, sum2) || rec(nums, index + 1, sum1, sum2 + tmp));
    }
}
