//codingbat.com/prob/p142270

public class Warmup2__stringTimes {
    public void Start()
    {
        System.out.println("\n");
        System.out.println(stringTimes("Oh Boy!", 2));
        System.out.println(stringTimes("x", 4));
        System.out.println(stringTimes("Hi", 5));
    }

    public String stringTimes(String str, int n) {
        String s = "";
        for(int i = 0;i<n;i++)
        {
            s += str;
        }
        return s;
    }

}
