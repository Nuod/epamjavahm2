//codingbat.com/prob/p184496

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Functional2__no34 {

    public void Start()
    {
        System.out.println("\n");
        System.out.println(no34(Arrays.asList("a", "bb", "ccc")));
        System.out.println(no34(Arrays.asList("ccc", "dddd", "apple")));
        System.out.println(no34(Arrays.asList("")));
    }

    public List<String> no34(List<String> strings) {
        List<String> res = new ArrayList<String>();
        for (String itr: strings) {
            if (itr.length() != 3 && itr.length() != 4)
                res.add(itr);
        }
        return  res;
    }



}
